![alt text](https://bitbucket.org/chayapolassawachaisuwikom/shortest-path/raw/0fae0dfe23b5d44e0175926d00d19adfc8fe21ff/img/system-screen-shot.png)

# Setup and install #
We have 2 options for install this system
1.  Install with docker compose
2.  Manual install  without docker compose

## Install with docker compose ##
1.  Config environment of the system on file `docker/.env`

Exmple
```
DATABASE_NAME=central-tech
DATABASE_USER=central-tech
DATABASE_PASSWORD=1234
DATABASE_ROOT_PASSWORD=centraladmin1234

APP_ENV=prod
APP_SECRET=24e17c47430bd2044a61c131c1cf6990

```
2.  Config Test DB on `DATABASE_URL` param in file `src\phpunit.xml.dist`
```xml
<env name="DATABASE_URL" value="mysql://central-tech:1234@192.168.99.100:3306/central-tech?serverVersion=5.7" force="true"/>
```
3.  Run docker command to build the system
```
docker-compose up
```

4.  Run Test

```
docker-compose run php-fpm bin/phpunit --coverage-html build ./tests

```
![alt text](https://bitbucket.org/chayapolassawachaisuwikom/shortest-path/raw/0fae0dfe23b5d44e0175926d00d19adfc8fe21ff/img/unit-test.png)

5.  Create Routes CSV file and setup to the system
```
CSV format (no header row):
A,B,10
B,C,5
A,C,17
B,D,11'
```
```
docker-compose run php-fpm php bin/console route:csv --file="csv/routes.csv"
```
## Manual install  without docker compose ##
1.  Install PHP version7.3 or above
2.  Install webserver
3.  Install Mysql and create a new DB for the system
4.  copy `src` folder to webserver directory
5.  Config environment of the system on file `.env`
```
# In all environments, the following files are loaded if they exist,
# the latter taking precedence over the former:
#
#  * .env                contains default values for the environment variables needed by the app
#  * .env.local          uncommitted file with local overrides
#  * .env.$APP_ENV       committed environment-specific defaults
#  * .env.$APP_ENV.local uncommitted environment-specific overrides
#
# Real environment variables win over .env files.
#
# DO NOT DEFINE PRODUCTION SECRETS IN THIS FILE NOR IN ANY OTHER COMMITTED FILES.
#
# Run "composer dump-env prod" to compile .env files for production use (requires symfony/flex >=1.2).
# https://symfony.com/doc/current/best_practices/configuration.html#infrastructure-related-configuration

###> symfony/framework-bundle ###
#APP_ENV=dev
#APP_SECRET=24e17c47430bd2044a61c131c1cf6990
#TRUSTED_PROXIES=127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
#TRUSTED_HOSTS='^localhost|example\.com$'
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# For a PostgreSQL database, use: "postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8"
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
DATABASE_URL=mysql://central-tech:1234@192.168.99.100:3306/central-tech?serverVersion=5.7
###< doctrine/doctrine-bundle ###

```
6.  Install dependencies
```
composer install
```
7.  Migrate DB
```
bin/console doctrine:migrations:migrate
```
8.  Run Test
```
bin/phpunit --coverage-html build ./tests
```
![alt text](https://bitbucket.org/chayapolassawachaisuwikom/shortest-path/raw/0fae0dfe23b5d44e0175926d00d19adfc8fe21ff/img/unit-test.png)

9.  Create Routes CSV file and setup to the system
```
CSV format (no header row):
A,B,10
B,C,5
A,C,17
B,D,11'
```
```
php bin/console route:csv --file="csv/routes.csv
```
