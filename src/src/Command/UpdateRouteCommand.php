<?php

namespace App\Command;

use App\Service\RouteManageInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command to update the route information
 */
class UpdateRouteCommand extends Command
{
    protected static $defaultName = 'route:csv';

    private $rm;

    public function __construct(RouteManageInterface $rm)
    {
        parent::__construct();
        $this->rm        = $rm;
    }

    /**
     * Configure function for UpdateRouteCommand
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this
            ->setDescription('Input vertex and edge using CSV file')
            ->setHelp(
                'This command allows you to update a route
using csv file path to input --file=/full/path/to/routes.csv
CSV format (no header row):
A,B,10
B,C,5
A,C,17
B,D,11'
            )
            ->addOption('file', null, InputOption::VALUE_REQUIRED, 'CSV file path');
    }


    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getOption('file');
        if (!$filePath) {
            $output->writeln('Please input file path on --file');
        } else if (!file_exists($filePath)) {
            $output->writeln('`' . $filePath . '` is not exist');
        } else {
            try {
                $fileContent = file_get_contents($filePath);
                $routes      = $this->rm->createArrayFromCSV($fileContent);
                $massage     = $this->rm->saveToDB($routes);
                $output->writeln($massage);
            } catch (\Exception $e) {
                $output->writeln('The file format is incorrect !!');
            }
        }

        return 0;
    }
}
