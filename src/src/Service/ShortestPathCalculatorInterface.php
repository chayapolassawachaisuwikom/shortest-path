<?php
declare(strict_types = 1);

namespace App\Service;
use App\Classes\ShortestResult;
use App\Entity\Edge;

/**
 * Interface for Shortest Path Calculator Interface
 */
interface ShortestPathCalculatorInterface
{
    /**
     * @param string $origin
     * @param string $destination
     *
     * @return ShortestResult
     */
    public function calculate(string $origin, string $destination): ShortestResult;

    /**
     * Setup a new Route
     * @param Edge[] $edges
     */
    public function setRoute(array $edges): void;
}
