<?php

namespace App\Service;

use App\Classes\ShortestResult;
use App\Entity\Edge;

class DijkstraAlgorithm implements ShortestPathCalculatorInterface
{
    private $routes;

    /**
     * Calculate shortest path
     *
     * @param string $origin
     * @param string $destination
     *
     * @return ShortestResult
     */
    public function calculate(string $origin, string $destination): ShortestResult
    {
        $result       = new ShortestResult();
        $result->from = $origin;
        $result->to   = $destination;
        if (!$this->routes) {
            return $result;
        }

        //initialize the array for storing
        $nearestPath = [];//the nearest path with its parent and weight
        $leftNode    = [];//the left nodes without the nearest path
        foreach (array_keys($this->routes) as $val) {
            $leftNode[$val] = 99999;
        }
        $leftNode[$origin] = 0;

        //start calculating
        while (!empty($leftNode)) {
            $min = array_search(min($leftNode), $leftNode);//the most min weight
            if ($min === $destination) {
                break;
            }
            foreach ($this->routes[$min] as $key => $val) {
                if (!empty($leftNode[$key]) && $leftNode[$min] + $val < $leftNode[$key]) {
                    $leftNode[$key]    = $leftNode[$min] + $val;
                    $nearestPath[$key] = [$min, $leftNode[$key]];
                }
            }
            unset($leftNode[$min]);
        }

        //list the path
        $path = [];
        $pos  = $destination;
        while ($pos !== $origin) {
            $path[] = trim($pos);
            $pos    = $nearestPath[$pos][0];
        }
        $path[] = $origin;
        $path   = array_reverse($path);

        $result->path    = implode('->', $path);
        $result->length  = $nearestPath[$destination][1];

        return $result;
    }

    /**
     * @param Edge[] $edges
     */
    public function setRoute(array $edges): void
    {
        foreach ($edges as $edge) {
            $this->routes[$edge->getOrigin()->getName()][$edge->getDestination()->getName()] = $edge->getLength();
            $this->routes[$edge->getDestination()->getName()][$edge->getOrigin()->getName()] = $edge->getLength();
        }
    }
}
