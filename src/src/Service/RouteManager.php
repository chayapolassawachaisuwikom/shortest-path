<?php

namespace App\Service;

use App\Entity\Edge;
use App\Entity\Vertex;
use Doctrine\ORM\EntityManagerInterface;

class RouteManager implements RouteManageInterface
{

    private $em;

    /**
     * RouteManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * Create array from CSV string
     *
     * @param string $csvContent
     *
     * @return array
     */
    public function createArrayFromCSV(string $csvContent): array
    {
        $csvContent= preg_replace('/[\x80-\xFF]/', '',$csvContent);
        $lines = explode(PHP_EOL, trim($csvContent));
        $array = [];
        foreach ($lines as $line) {
            $array[] = str_getcsv($line);
        }

        return $array;
    }

    /**
     * @param array $routes
     *
     * @return ?string
     */
    public function saveToDB(array $routes): ?string
    {
        $this->clearRoute();
        $newNode    = 0;
        $newEdge    = 0;
        $updateEdge = 0;
        foreach ($routes as $route) {
            list($originName, $destinationName, $length) = $route;
            if ($originName === $destinationName) {
                continue;
            }
            $origin      = $this->em->getRepository(Vertex::class)->findOneBy(['name' => $originName]);
            $destination = $this->em->getRepository(Vertex::class)->findOneBy(['name' => $destinationName]);

            if (!$origin) {
                $origin = new Vertex();
                $origin->setName($originName);
                $this->em->persist($origin);
                $this->em->flush();
                $newNode++;
            }
            if (!$destination) {
                $destination = new Vertex();
                $destination->setName($destinationName);
                $this->em->persist($destination);
                $this->em->flush();
                $newNode++;
            }
            $edge  = $this->em->getRepository(Edge::class)->findOneBy(
                ['origin' => $origin, 'destination' => $destination]
            );
            $edge2 = $this->em->getRepository(Edge::class)->findOneBy(
                ['origin' => $destination, 'destination' => $origin]
            );
            if (!$edge && !$edge2) {
                $edge = new Edge();
                $edge->setOrigin($origin);
                $edge->setDestination($destination);
                $edge->setLength($length);
                $this->em->persist($edge);
                $newEdge++;
            } else {
                if ($edge) {
                    $edge->setLength($length);
                } else if ($edge2) {
                    $edge2->setLength($length);
                }
                $updateEdge++;
            }
            $this->em->flush();
        }

        return $newNode . ' node have been created.
' . $newEdge . ' edge have been created.
' . $updateEdge . ' edge have been updated.';
    }

    /**
     * Clear Routes
     */
    public function clearRoute(): void
    {
        $origins      = $this->em->getRepository(Vertex::class)->findAll();
        $destinations = $this->em->getRepository(Vertex::class)->findAll();
        $edges        = $this->em->getRepository(Edge::class)->findAll();

        // Clear Test DB
        foreach ($origins as $o) {
            $this->em->remove($o);
        }
        foreach ($destinations as $d) {
            $this->em->remove($d);
        }
        foreach ($edges as $e) {
            $this->em->remove($e);
        }
        $this->em->flush();
    }
}
