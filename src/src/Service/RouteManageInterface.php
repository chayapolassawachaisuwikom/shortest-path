<?php
declare(strict_types = 1);

namespace App\Service;

/**
 * Interface for route manager
 */
interface RouteManageInterface
{
    /**
     * Create array from CSV string
     * @param string $csvContent
     *
     * @return array
     */
    public function createArrayFromCSV(string $csvContent): array;

    /**
     * Import array data to database
     * @param array $routes
     *
     * @return null|string
     */
    public function saveToDB(array $routes): ?string;
}
