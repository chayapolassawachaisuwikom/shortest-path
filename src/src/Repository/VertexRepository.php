<?php

namespace App\Repository;

use App\Entity\Vertex;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Vertex|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vertex|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vertex[]    findAll()
 * @method Vertex[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VertexRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vertex::class);
    }

    // /**
    //  * @return Vertex[] Returns an array of Vertex objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vertex
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
