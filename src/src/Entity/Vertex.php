<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VertexRepository")
 */
class Vertex
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Edge", mappedBy="origin")
     */
    private $edges;

    public function __construct()
    {
        $this->edges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Edge[]
     */
    public function getEdges(): Collection
    {
        return $this->edges;
    }

    public function addEdge(Edge $edge): self
    {
        if (!$this->edges->contains($edge)) {
            $this->edges[] = $edge;
            $edge->setOrigin($this);
        }

        return $this;
    }

    public function removeEdge(Edge $edge): self
    {
        if ($this->edges->contains($edge)) {
            $this->edges->removeElement($edge);
            if ($edge->getOrigin() === $this) {
                $edge->setOrigin(null);
            }
        }

        return $this;
    }
}
