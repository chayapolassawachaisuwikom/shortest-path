<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EdgeRepository")
 */
class Edge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vertex", inversedBy="edges")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $origin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vertex", inversedBy="edges")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $destination;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Assert\Positive
     */
    private $length;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrigin(): ?Vertex
    {
        return $this->origin;
    }

    public function setOrigin(?Vertex $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getDestination(): ?Vertex
    {
        return $this->destination;
    }

    public function setDestination(?Vertex $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function setLength($length): self
    {
        $this->length = $length;

        return $this;
    }
}
