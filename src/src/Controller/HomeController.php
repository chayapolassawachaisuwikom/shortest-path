<?php

namespace App\Controller;

use App\Entity\Edge;
use App\Entity\Vertex;
use App\Repository\PostRepository;
use App\Service\ShortestPathCalculatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="")
     * @Template
     */
    public function index()
    {
        $em      = $this->getDoctrine()->getManager();
        $edges   = $em->getRepository(Edge::class)->findAll();
        $vertexs = $em->getRepository(Vertex::class)->findAll();

        return [
            'edges'   => $edges,
            'vertexs' => $vertexs
        ];
    }

    /**
     * @Route("/api/get-shortest-path", name="get_shortest_path")
     */
    public function getShortestPath(ShortestPathCalculatorInterface $shortestPathCalculator, Request $request)
    {
        $from = $request->query->get('from');
        $to   = $request->query->get('to');

        if ($from === $to) {
            $response = ['success' => false, 'message' => 'From and To should be different value'];

            return $this->json($response);
        }
        $em    = $this->getDoctrine()->getManager();
        $edges = $em->getRepository(Edge::class)->findAll();

        $shortestPathCalculator->setRoute($edges);
        try {
            $shortestPath = $shortestPathCalculator->calculate($from, $to);

            return $this->json(['success' => true, 'result' => $shortestPath]);
        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => 'No path from ' . $from . ' to ' . $to];

            return $this->json($response);
        }
    }
}
