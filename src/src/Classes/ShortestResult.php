<?php
namespace App\Classes;

class ShortestResult
{
    public $success = false;
    public $from = '';
    public $to = '';
    public $length = 0;
    public $path = '';
}
