<?php

namespace App\Tests\Command;

use App\Service\RouteManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{

    public function testHomePage()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('html body', 'Shortest Path Finding');
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $response
     * @dataProvider shortestPathDataProvider
     */
    public function testAPI(string $from, string $to, string $response)
    {
        $client = static::createClient();
        $rm     = self::$container->get(RouteManager::class);
        $rm->saveToDB(
            [
                ['A', 'B', 4],
                ['B', 'C', 5]
            ]
        );

        $client->request('GET', "/api/get-shortest-path?from=$from&to=$to");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(
            $response,
            $client->getResponse()->getContent()
        );
        $rm->clearRoute();
    }

    public function shortestPathDataProvider()
    {
        return [
            'Success' => [
                'from' => 'A',
                'to' => 'C',
                'response' => '{"success":true,"result":{"success":false,"from":"A","to":"C","length":9,"path":"A-\u003EB-\u003EC"}}'
            ],
            'From and To same' => [
                'from' => 'A',
                'to' => 'A',
                'response' => '{"success":false,"message":"From and To should be different value"}'
            ],
            'Cannot calculate' => [
                'from' => 'A',
                'to' => 'G',
                'response' => '{"success":false,"message":"No path from A to G"}'
            ],
        ];
    }
}
