<?php

namespace App\Tests\Command;

use App\Service\RouteManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class UpdateRouteCommandTest extends KernelTestCase
{

    public function testSuccessFormat()
    {
        $kernel      = static::bootKernel();
        $rm          = $kernel->getContainer()->get(RouteManager::class);
        $application = new Application($kernel);

        $command       = $application->find('route:csv');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--file' => 'csv/routes.csv',
            ]
        );
        $output = $commandTester->getDisplay();
        $rm->clearRoute();
        $this->assertStringContainsString('4 node have been created', $output);
    }

    public function testInvalidFileFormat()
    {
        $kernel      = static::createKernel();
        $application = new Application($kernel);

        $command       = $application->find('route:csv');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--file' => 'csv/routes-failed.csv',
            ]
        );
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('The file format is incorrect !!', $output);
    }

    public function testInvalidPath()
    {
        $kernel      = static::createKernel();
        $application = new Application($kernel);

        $command       = $application->find('route:csv');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--file' => '1234.csv',
            ]
        );
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('`1234.csv` is not exist', $output);
    }

    public function testBlankInput()
    {
        $kernel      = static::createKernel();
        $application = new Application($kernel);

        $command       = $application->find('route:csv');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--file' => '',
            ]
        );

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Please input file path on --file', $output);
    }
}
