<?php

namespace App\Tests\Command;

use App\Entity\Edge;
use App\Service\DijkstraAlgorithm;
use App\Service\RouteManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DijkstraAlgorithmTest extends KernelTestCase
{

    /**
     * @param array  $route
     * @param string $from
     * @param string $to
     * @param int    $disance
     *
     * @dataProvider shortestPathDataProvider
     */
    public function testExecute(array $route, string $from, string $to, int $disance)
    {
        $kernel = static::bootKernel();
        $rm     = $kernel->getContainer()->get(RouteManager::class);
        $em     = $kernel->getContainer()->get('doctrine')->getManager();
        $da     = $kernel->getContainer()->get(DijkstraAlgorithm::class);

        $rm->saveToDB($route);
        $edges = $em->getRepository(Edge::class)->findAll();
        $da->setRoute($edges);
        $result = $da->calculate($from, $to);
        $this->assertEquals($disance, $result->length);
        $rm->clearRoute();
    }

    public function shortestPathDataProvider()
    {
        return [
            'Empty Map' => [
                'routes'   => [],
                'from'     => 'A',
                'to'       => 'C',
                'distance' => 0
            ],
            'Simple Case' => [
                'routes'   => [
                    ['A', 'B', 4],
                    ['B', 'C', 5]
                ],
                'from'     => 'A',
                'to'       => 'C',
                'distance' => 9
            ],
            'Complex Case' => [
                'routes'   => [
                    ['A', 'B', 7],
                    ['A', 'F', 9],
                    ['A', 'E', 14],
                    ['B', 'F', 8],
                    ['B', 'C', 15],
                    ['C', 'F', 6],
                    ['C', 'D', 4],
                    ['D', 'E', 9],
                    ['E', 'F', 2],
                ],
                'from'     => 'A',
                'to'       => 'D',
                'distance' => 19
            ],
            'Complex Case 2' => [
                'routes'   => [
                    ['A', 'B', 7],
                    ['A', 'F', 9],
                    ['A', 'E', 14],
                    ['B', 'F', 8],
                    ['B', 'C', 15],
                    ['C', 'F', 6],
                    ['C', 'D', 4],
                    ['D', 'E', 9],
                    ['E', 'F', 2],
                ],
                'from'     => 'A',
                'to'       => 'E',
                'distance' => 11
            ]
        ];
    }
}
