<?php

namespace App\Tests\Command;

use App\Entity\Edge;
use App\Entity\Vertex;
use App\Service\RouteManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RouteManagerTest extends KernelTestCase
{

    public function testSetRoute()
    {
        $kernel       = static::bootKernel();
        $rm           = $kernel->getContainer()->get(RouteManager::class);
        $em           = $kernel->getContainer()->get('doctrine')->getManager();

        $rm->saveToDB(
            [
                ['A', 'B', 1]
            ]
        );
        $rm->saveToDB(
            [
                ['A', 'B', 4],
                ['B', 'C', 5],
                ['A', 'A', 1]
            ]
        );
        $origin      = $em->getRepository(Vertex::class)->findOneBy(['name' => 'A']);
        $destination = $em->getRepository(Vertex::class)->findOneBy(['name' => 'B']);
        $edge        = $em->getRepository(Edge::class)->findOneBy(['origin' => $origin, 'destination' => $destination]);

        $this->assertEquals('A', $origin->getName());
        $this->assertEquals('B', $destination->getName());
        $this->assertEquals(4, $edge->getLength());
        $this->assertCount(2, $em->getRepository(Edge::class)->findAll());
        $rm->clearRoute();
    }

    public function testClearData()
    {
        $kernel       = static::bootKernel();
        $rm           = $kernel->getContainer()->get(RouteManager::class);
        $em           = $kernel->getContainer()->get('doctrine')->getManager();

        $rm->saveToDB(
            [
                ['A', 'B', 1]
            ]
        );
        $rm->clearRoute();
        $this->assertCount(0, $em->getRepository(Edge::class)->findAll());
        $em->close();
        $em = null;
    }
}
